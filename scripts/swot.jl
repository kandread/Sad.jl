### Run SAD algorithm with SWOT data

using Sad
using Distributions
using NCDatasets

"""
Load SWOT observations.
"""
function read_swot_obs(ncfile)
    ds = Dataset(ncfile)
    nodes = NCDatasets.group(ds, "node")
    S = nodes["slope2"][:]
    H = nodes["wse"][:]
    W = nodes["width"][:]
    H, W, S
end

"""
Extract river channel chainage.
"""
function channel_chainage(H, W, S)
    # sort nodes from downstream to upstream
    # FIXME: currently uses the mean water surface elevation
    hm = [mean(skipmissing(H[:, c])) for c in 1:size(H, 2)]
    i = sortperm(hm)
    H = H[:, i]'
    W = W[:, i]'
    S = S[:, i]'
    j = [v[1] for v in findall(all(!ismissing, H, dims=2))]
    H = convert(Array{Float64, 2}, H[j, :])
    W = convert(Array{Float64, 2}, W[j, :])
    S = convert(Array{Float64, 2}, S[j, :])
    # calculate distance from downstream node
    # FIXME: CSV with geographical information does not correspond directly to the nodes in each SWOT
    # orbit pass file. We can't use the calculated slope do derive the distance between
    # cross-sections as there are rather implausible discontinuities in the test data.
    # For now, just assume that cross-sections are 100 m apart
    x = (collect(0:size(H, 1)) * 100.0)[1:end-1]
    H, W, S, x
end

"""
Main driver routine.
"""
function main()
    swotfile = ARGS[1] # NetCDF file with SWOT observations
    swordfile = ARGS[2] # NetCDF file with SWORD database 
    H, W, S = read_swot_obs(swotfile)
    H, W, S, x = channel_chainage(H, W, S)
    Qₚ, nₚ, rₚ, zₚ = Sad.priors(swordfile, H)
    nens = 100 # default ensemble size
    A0, n = Sad.assimilate(H, W, x, maximum(W, dims=2), maximum(H, dims=2), S,
                           Qₚ, nₚ, rₚ, zₚ, nens, [1, length(x)])
    println("$(A0[1]), $(n[1])")
end

main()
